/*
Navicat MySQL Data Transfer

Source Server         : MySQL MAMP
Source Server Version : 50634
Source Host           : localhost:3306
Source Database       : e_presents

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2017-12-20 15:52:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for absensi_dosen
-- ----------------------------
DROP TABLE IF EXISTS `absensi_dosen`;
CREATE TABLE `absensi_dosen` (
  `id_absensi` int(11) NOT NULL AUTO_INCREMENT,
  `rf_id` varchar(15) NOT NULL,
  `kd_jadwal` varchar(10) NOT NULL,
  `waktu_absen` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_inval` char(1) DEFAULT NULL,
  `id_dp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_absensi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of absensi_dosen
-- ----------------------------

-- ----------------------------
-- Table structure for absensi_dosen_out
-- ----------------------------
DROP TABLE IF EXISTS `absensi_dosen_out`;
CREATE TABLE `absensi_dosen_out` (
  `id_absensi_out` int(11) NOT NULL AUTO_INCREMENT,
  `rf_id` varchar(15) NOT NULL,
  `kd_jadwal` varchar(10) NOT NULL,
  `waktu_absen` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_inval` char(1) DEFAULT NULL,
  `id_dp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_absensi_out`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of absensi_dosen_out
-- ----------------------------

-- ----------------------------
-- Table structure for absensi_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `absensi_mahasiswa`;
CREATE TABLE `absensi_mahasiswa` (
  `id_absensi` int(11) NOT NULL AUTO_INCREMENT,
  `rf_id` varchar(15) NOT NULL,
  `kd_jadwal` varchar(10) NOT NULL,
  `waktu_absen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_absensi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of absensi_mahasiswa
-- ----------------------------

-- ----------------------------
-- Table structure for data_blacklist
-- ----------------------------
DROP TABLE IF EXISTS `data_blacklist`;
CREATE TABLE `data_blacklist` (
  `id_blacklist` int(11) NOT NULL AUTO_INCREMENT,
  `rf_id` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_blacklist`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_blacklist
-- ----------------------------

-- ----------------------------
-- Table structure for data_dosen
-- ----------------------------
DROP TABLE IF EXISTS `data_dosen`;
CREATE TABLE `data_dosen` (
  `id_dosen` varchar(15) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama_dosen` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_dosen
-- ----------------------------

-- ----------------------------
-- Table structure for data_mata_kuliah
-- ----------------------------
DROP TABLE IF EXISTS `data_mata_kuliah`;
CREATE TABLE `data_mata_kuliah` (
  `kd_mata_kuliah` varchar(10) NOT NULL,
  `nama_mata_kuliah` varchar(50) NOT NULL,
  `sks` int(11) NOT NULL,
  PRIMARY KEY (`kd_mata_kuliah`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_mata_kuliah
-- ----------------------------

-- ----------------------------
-- Table structure for errorhandle
-- ----------------------------
DROP TABLE IF EXISTS `errorhandle`;
CREATE TABLE `errorhandle` (
  `kode` varchar(50) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='berisi data kode kode error';

-- ----------------------------
-- Records of errorhandle
-- ----------------------------
INSERT INTO `errorhandle` VALUES ('0112', 'Tidak dapat melakukan absensi');
INSERT INTO `errorhandle` VALUES ('0113', 'Koneksi Database Gagal');
INSERT INTO `errorhandle` VALUES ('0114', 'Tidak dapat melakukan tindakan tersebut,dikarenakan server error');
INSERT INTO `errorhandle` VALUES ('0112', 'Tidak dapat melakukan absensi');
INSERT INTO `errorhandle` VALUES ('0113', 'Koneksi Database Gagal');
INSERT INTO `errorhandle` VALUES ('0114', 'Tidak dapat melakukan tindakan tersebut,dikarenakan server error');
INSERT INTO `errorhandle` VALUES ('0112', 'Tidak dapat melakukan absensi');
INSERT INTO `errorhandle` VALUES ('0113', 'Koneksi Database Gagal');
INSERT INTO `errorhandle` VALUES ('0114', 'Tidak dapat melakukan tindakan tersebut,dikarenakan server error');

-- ----------------------------
-- Table structure for rf_id_users
-- ----------------------------
DROP TABLE IF EXISTS `rf_id_users`;
CREATE TABLE `rf_id_users` (
  `rf_id` varchar(15) NOT NULL,
  `no_induk` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL,
  `time_expired` int(50) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rf_id_users
-- ----------------------------
INSERT INTO `rf_id_users` VALUES ('0002226461', '43A87006150071', 'mahasiswa', '6400', null, '2017-12-20 12:38:04', '2017-12-20 12:38:04');
INSERT INTO `rf_id_users` VALUES ('0002266939', '43A87006150100', 'mahasiswa', '6400', null, '2017-12-18 14:31:49', '2017-12-18 13:31:49');
INSERT INTO `rf_id_users` VALUES ('0002267996', '43A87006150224', 'mahasiswa', '6400', null, '2017-12-18 14:31:17', '2017-12-18 13:31:17');
INSERT INTO `rf_id_users` VALUES ('0004456494', 'D1209', 'dosen', '6400', null, '2017-12-18 01:30:50', '2017-12-17 18:30:50');
INSERT INTO `rf_id_users` VALUES ('0004459487', 'D0110', 'dosen', '6400', null, '2017-12-18 15:21:28', '2017-12-18 15:21:28');

-- ----------------------------
-- Table structure for tb_aksi_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_aksi_token`;
CREATE TABLE `tb_aksi_token` (
  `id_aksi` int(11) NOT NULL,
  `keterangan_aksi` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_aksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_aksi_token
-- ----------------------------
INSERT INTO `tb_aksi_token` VALUES ('1', 'Absen Masuk Tidak Membawa Kartu');
INSERT INTO `tb_aksi_token` VALUES ('2', 'Absen Jadwal Tambahan');
INSERT INTO `tb_aksi_token` VALUES ('3', 'Dosen Di Inval');
INSERT INTO `tb_aksi_token` VALUES ('4', 'Terlambat');

-- ----------------------------
-- Table structure for tb_operators
-- ----------------------------
DROP TABLE IF EXISTS `tb_operators`;
CREATE TABLE `tb_operators` (
  `id_operator` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `nama_operator` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_operator`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_operators
-- ----------------------------
INSERT INTO `tb_operators` VALUES ('1', 'jois', '$2y$10$ew3fMEOqJi72P0bxhiUvGOzNEaJMflzT3QkzZiz/3av9I9dGiOMOi', 'Jois Andresky', '2017-12-17 22:51:14', '2017-12-17 22:51:14', null);
INSERT INTO `tb_operators` VALUES ('2', 'jois77', '$2y$10$GUnjcN59Gg2k8UiZ6MxXEeojWwZP/leoaNDs39knA09gQBfc8x25u', 'Jois Aja', '2017-12-09 18:43:44', '2017-12-09 18:43:44', null);

-- ----------------------------
-- Table structure for tb_session
-- ----------------------------
DROP TABLE IF EXISTS `tb_session`;
CREATE TABLE `tb_session` (
  `id` varchar(50) NOT NULL,
  `key` varchar(50) DEFAULT NULL,
  `val` varchar(20000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_session
-- ----------------------------

-- ----------------------------
-- Table structure for users_token
-- ----------------------------
DROP TABLE IF EXISTS `users_token`;
CREATE TABLE `users_token` (
  `token_id` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `used` varchar(1) NOT NULL,
  `id_aksi` int(11) NOT NULL,
  `rf_id` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `no_induk` varchar(20) NOT NULL,
  `nidp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users_token
-- ----------------------------
INSERT INTO `users_token` VALUES ('6916262012124', '2017-12-20 05:29:35', '2017-12-20 05:29:35', 'N', '2', '123454', 'mahasiswa', '43A87006150593', '-');
INSERT INTO `users_token` VALUES ('6917291814330', '2017-12-18 07:35:28', '2017-12-18 07:35:28', 'N', '1', '0004456494', 'dosen', 'D1209', '-');
INSERT INTO `users_token` VALUES ('6919321815485', '2017-12-18 08:25:56', '2017-12-18 08:25:56', 'N', '3', '0004456494', 'dosen', 'D1209', 'D0110');
INSERT INTO `users_token` VALUES ('692120201383', '2017-12-19 18:31:40', '2017-12-19 18:31:40', 'N', '3', '0004459487', 'dosen', 'D0110', 'D1209');
