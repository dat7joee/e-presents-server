<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>E-PRESENTS STMIK Bani Saleh</title>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- Bootstrap core CSS     -->
    <link href="{{ URL::to('assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ URL::to('assets/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ URL::to('assets/css/light-bootstrap-dashboard.css?v=1.4.0') }}" rel="stylesheet"/>

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'> -->
    <link href="{{ URL::to('assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
  </head>
  <body>
    <div id="app"></div>
    <script src="{{ URL::to('dist/build.js') }}"></script>
    <!--   Core JS Files   -->
    <script src="{{ URL::to('assets/js/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
  </body>
</html>