<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\JWTException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            $token = JWTAuth::getToken();
            if (!$token) {
                return $this->response->errorMethodNotAllowed('Token not provided');
            }
            try {
                $refreshedToken = JWTAuth::refresh($token);
            } catch (JWTException $e) {
                return $this->response->errorInternal('Not able to refresh Token');
            }
            return response()->json(['error'=>'token_expired','token'=>$refreshedToken]);
            // try
            // {
            //     $refreshed = JWTAuth::refresh(JWTAuth::getToken());
            //     $user = JWTAuth::setToken($refreshed)->toUser();
            //     header('Authorization: Bearer ' . $refreshed);
            // }
            // catch (JWTException $e)
            // {
            //      return response()->json([
            //        'code'   => 103 // means not refreshable
            //        'response' => null // nothing to show
            //      ]);
            // }
            // return response()->json(['error' => 'token_expired'], $e->getStatusCode());
        } else if ($e instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return response()->json(['error' => 'token_invalid'], $e->getStatusCode());
        } else if($e instanceof JWTException){
            return response()->json(['error' => 'error_fetch_token'], $e->getStatusCode());
        }
        return parent::render($request, $e);
    }
}
