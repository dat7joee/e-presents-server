<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenCard extends Model
{
    protected $table = 'users_token';
    protected $primaryKey = 'token_id';
    protected $fillable = [
        'token_id', 'used', 'id_aksi', 'rf_id', 'status', 'no_induk', 'nidp'
    ];
}
