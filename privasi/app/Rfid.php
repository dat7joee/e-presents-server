<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rfid extends Model
{
    protected $table = 'rf_id_users';
    protected $primaryKey = 'rf_id';
    protected $fillable = ['rf_id', 'no_induk', 'status', 'time_expired', 'created_at', 'updated_at', 'remember_token'];
    protected $hidden = ['remember_token'];
}
