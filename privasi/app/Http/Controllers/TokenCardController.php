<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\TokenCard;
use DB;

class TokenCardController extends Controller
{
    public function view(Request $request){
        $tokenCard = DB::table('users_token')->leftjoin('tb_aksi_token', 'users_token.id_aksi', '=','tb_aksi_token.id_aksi')->where('no_induk', 'LIKE', '%'.$request->input('search').'%')
            ->orderBy('created_at', 'desc')->paginate(10);
        return response()->json($tokenCard, 200);
    }

    public function cariDosen(Request $request) {
      $ds = DB::connection('mysql_bansal')->table('tbdosaktf')->where('DSNAMA', 'LIKE', '%'.$request->input('search').'%')->orderBy('DSNAMA', 'asc')->limit(5)->get();
      return response()->json($ds, 200);
    }

    public function cariNI (Request $request) {
        $dosen = DB::connection('mysql_bansal')->table('tbdosaktf')->where('DSNAMA', 'LIKE', '%'.$request->input('keywords').'%')->orderBy('DSNAMA', 'asc')->limit(3)->get();
        $mahasiswa = DB::connection('mysql_bansal')->table('msmhs')->select('NIMHSCMSMHS as NPM', 'NMMHSCMSMHS as Nama')->where('NMMHSCMSMHS', 'LIKE', '%'.$request->input('keywords').'%')->orderBy('NMMHSCMSMHS', 'asc')->limit(3)->get();
        return response()->json(['mahasiswa' => $mahasiswa, 'dosen' => $dosen], 200);
        // if($dosen && $mahasiswa){
        //     return response()->json(['success' => true, 'mahasiswa' => $mahasiswa, 'dosen' => $dosen]);
        // }

        // return response()->json(['success' => false, 'mahasiswa' => [], 'dosen' => []]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'token_id' => 'required|unique:users_token',
            'id_aksi' => 'required',
            'rf_id' => 'required',
            'status' => 'required',
            'no_induk' => 'required',
            'nidp' => 'required'
        ]);

        $newTokenCard = new TokenCard([
            'token_id' => $request->input('token_id'),
            'used' => 'N',
            'id_aksi' => $request->input('id_aksi'),
            'rf_id' => $request->input('rf_id'),
            'status' => $request->input('status'),
            'no_induk' => $request->input('no_induk'),
            'nidp' => $request->input('nidp')
        ]);
        if($newTokenCard->save()) {
            return response()->json(['msg' => 'Berhasil Menambahkan Data!', 'success' => true], 201);
        }
        return response()->json(['msg' => 'Gagal Menambahkan Data!', 'success' => false], 400);
    }

    public function getDosenInval($id) {
        $dosen = DB::connection('mysql_bansal')->table('tbdosaktf')->select('DSNAMA as nama')->where('DSNOTBDOS', $id)->first();

        if($dosen) {
            return response()->json(['success' => true, 'dosen' => $dosen]);
        }

        return response()->json(['success' => false, 'msg' => 'Dosen Tidak Ditemukan!']);
    }

    public function update(Request $request, $id){
        $tokenCard = TokenCard::find($id);
        $this->validate($request, [
            'id_aksi' => 'required',
            'rf_id' => 'required',
            'status' => 'required',
            'no_induk' => 'required',
            'nidp' => 'required'
        ]);
        $tokenCard->id_aksi = $request->input('id_aksi');
        $tokenCard->rf_id = $request->input('rf_id');
        $tokenCard->status = $request->input('status');
        $tokenCard->no_induk = $request->input('no_induk');
        $tokenCard->nidp = $request->input('nidp');

        if($tokenCard->save()) {
            return response()->json(['msg' => 'Berhasil Mengubah Data!', 'success' => true], 200);
        }

        return response()->json(['msg' => 'Gagal Mengubah Data!', 'success' => false], 200);
    }

    public function useToken(Request $request, $id){
        $tokenCard = TokenCard::find($id);
        $tokenCard->used = 'Y';
        if($tokenCard->save()) {
            return response()->json(['msg' => 'Token Used!', 'success' => true], 200);
        }
        return response()->json(['msg' => 'Token Failed Used', 'success' => false], 200);
    }

    public function delete($id){
        $tokenCard = TokenCard::find($id);
        if($tokenCard->delete()){
            return response()->json(['msg' => 'Berhasil Menghapus Token!', 'success' => true], 200);
        }
        return response()->json(['msg' => 'Berhasil Menghapus Token!', 'success' => false], 200);
    }

    public function getAksiToken(){
        $aksi = DB::table('tb_aksi_token')->get();

        return response()->json($aksi, 200);
    }
}
