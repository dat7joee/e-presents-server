<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use \App\Rfid;
use Carbon\Carbon;

class RfidController extends Controller
{
    public function view(Request $request){
        $mhs = DB::table('rf_id_users')->where('no_induk', 'LIKE', '%'.$request->input('search').'%')->paginate(10);
        return response()->json($mhs, 200);
    }

    public function getRfidData($id){
        $ni = $id[0];
        switch ($ni) {
            case "D":
                $personal = DB::connection('mysql_bansal')->table('tbdosaktf')->select('DSNOTBDOS as NID', 'DSNAMA as Nama')->where('DSNOTBDOS', $id)->first();
                break;

            default:
                $personal = DB::connection('mysql_bansal')->table('msmhs')->select('NIMHSCMSMHS as NPM', 'NMMHSCMSMHS as Nama')->where('NIMHSCMSMHS', $id)->first();
        }
        $rfid = DB::table('rf_id_users')->where('no_induk', $id)->first();
        if($rfid){
            return response()->json(['success' => true, 'rfid' => $rfid, 'personal' => $personal], 200);
        }

        return response()->json(['success' => false, 'personal' => $personal], 200);
    }

    // public function searchData (Request $request) {
    //     $dosen = DB::connection('mysql_bansal')->table('tbdosaktf')->select('DSNOTBDOS as NID', 'DSNAMA as Nama')->where('DSNOTBDOS', 'LIKE ', '%'.$request->input('keywords').'%')->limit(5)->get();
    //     $mahasiswa = DB::connection('mysql_bansal')->table('msmhs')->select('NIMHSCMSMHS as NPM', 'NMMHSCMSMHS as Nama')->where('NIMHSCMSMHS', 'LIKE', '%'.$request->input('keywords').'%')->limit(5)->get();

    //     if($dosen && $mahasiswa){
    //         return response()->json(['success' => true, 'mahasiswa' => $mahasiswa, 'dosen' => $dosen]);
    //     }

    //     return response()->json(['success' => false, 'mahasiswa' => [], 'dosen' => []]);
    // }

    public function getRfid($id) {
        $rfid = Rfid::find($id);

        return response()->json($rfid, 200);
    }

    public function store(Request $request){
        $this->validate($request, [
            'rf_id' => 'required',
            'no_induk' => 'required',
            'status' => 'required'
        ]);
        $rfid = new Rfid([
            'rf_id' => $request->input('rf_id'),
            'no_induk' => $request->input('no_induk'),
            'status' => $request->input('status'),
            'time_expired' => 64000
        ]);

        $rfid->save();
        return response()->json(['msg' => 'Berhasil Mendaftarkan RFID!'], 201);
    }

    public function update(Request $request, $id){
        // dd($id);
        $this->validate($request, [
            'no_induk' => 'required',
            'status' => 'required'
        ]);
        $rfid = Rfid::find($id);

        $rfid->no_induk = $request->input('no_induk');
        $rfid->status = $request->input('status');
        $rfid->updated_at = Carbon::now(8)->toDateTimeString();
        $rfid->save();

        return response()->json(['msg' => 'Berhasil Mengubah RFID!'], 200);
    }

    public function delete($id){
        $rfid = Rfid::find($id);
        $rfid->delete();

        return response()->json(['msg' => 'Berhasil Menghapus Rfid'], 200);
    }
}
