<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Blacklist;
use \App\Rfid;
use DB;

class BlacklistController extends Controller
{
    public function view(Request $request){
      $rfid = DB::table('data_blacklist')->join('rf_id_users', 'data_blacklist.rf_id', '=', 'rf_id_users.rf_id')->where('rf_id_users.no_induk', 'LIKE', '%'.$request->input('search').'%')->paginate(10);

      return response()->json($rfid, 200);
    }

    public function getCard($id){
      $rfid = Blacklist::find($id);

      return response()->json($rfid, 200);
    }

    public function store(Request $request){
      $this->validate($request, [
        'rfid' => 'required'
      ]);
      $checkRfid = Rfid::where('rf_id', $request->rfid)->first();
      if($checkRfid) {
        $checkIfExist = Blacklist::where('rf_id', $request->rfid)->first();
        if($checkIfExist) {
          return response()->json(['msg' => 'RFID  Tersebut Telah terblacklist!'], 500);
        } else {
          $rfid = new Blacklist([
            'rf_id' => $request->rfid
          ]);
          $rfid->save();
          return response()->json($rfid, 201);
        }
      } else {
        return response()->json(['msg' => 'RFID Tidak Terdaftar'], 500);
      }
    }

    public function delete($id){
      $rfid = Blacklist::find($id);
      $rfid->delete();

      return response()->json(['msg' => 'DELETED!', 'rfid' => $rfid], 200);
    }
}
