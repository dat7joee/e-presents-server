<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\User;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class UserController extends Controller
{
    public function register(Request $request){
        $this->validate($request, [
            'username' => 'required|unique:tb_operators',
            'password' => 'required',
            'nama_operator' => 'required',
            'role' => 'required'
        ]);

        $user = new User([
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'nama_operator' => $request->input('nama_operator'),
            'role' => $request->input('role')
        ]);
        $user->save();

        return response()->json(['msg' => 'Berhasil Mendaftarkan Operator'], 201);
    }

    public function login(Request $request){
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('username', 'password');
        try{
            if(!$token = JWTAuth::attempt($credentials)){
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }
        } catch(JWTException $e){
            return response()->json(['error' => 'Error Creating Token'], 500);
        }
        return response()->json(['token' => $token, 'user' => JWTAuth::toUser($token)], 200);
    }

    public function getProfile(){
        $operator = JWTAuth::toUser();

        return response()->json($operator, 200);
    }

    public function getOperators(Request $request) {
        $operator = DB::table('tb_operators')->where('nama_operator', 'LIKE', '%'.$request->input('search').'%')->paginate(15);

        return response()->json($operator, 200);
    }

    public function deleteOperator($id) {
        $operator = User::find($id)->delete();

        return response()->json(['success' => true], 200);
    }
}
