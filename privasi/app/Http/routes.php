<?php
Route::get('/', function(){
    return view('index');
});

Route::group(['middleware' => 'api', 'prefix' => 'api/v1'], function(){
    Route::group(['prefix' => 'operators'], function(){
        // Untuk Operator
        Route::get('/', [
            'uses' => 'UserController@getOperators',
            'middleware' => 'jwt.auth'
        ]);

        Route::post('/register', [
            'uses' => 'UserController@register'
        ]);

        Route::delete('/{id}', [
            'uses' => 'UserController@deleteOperator',
            'middleware' => 'jwt.auth'
        ]);

        Route::post('/login', [
            'uses' => 'UserController@login'
        ]);

        Route::get('/profile', [
            'uses' => 'UserController@getProfile',
            'middleware' => 'jwt.auth'
        ]);
    });

    Route::group(['middleware' => 'jwt.auth'], function(){

        // Untuk RFID Users
        Route::get('/rfid-users', [
            'uses' => 'RfidController@view'
        ]);

        Route::get('/rfid-users/{id}', [
            'uses' => 'RfidController@getRfidData'
        ]);

        Route::get('/rfid-users/edit/{id}', [
            'uses' => 'RfidController@getRfid'
        ]);

        Route::post('/rfid-users', [
            'uses' => 'RfidController@store',
            'middleware' => 'jwt.auth'
        ]);

        Route::put('/rfid-users/{id}', [
            'uses' => 'RfidController@update'
        ]);

        Route::delete('/rfid-users/{id}', [
            'uses' => 'RfidController@delete'
        ]);

        // Untuk RFID yang Di Blacklist
        Route::get('/rfid-blacklist', [
            'uses' => 'BlacklistController@view'
        ]);

        Route::get('/rfid-blacklist/{id}', [
            'uses' => 'BlacklistController@getCard'
        ]);

        Route::post('/rfid-blacklist', [
            'uses' => 'BlacklistController@store'
        ]);

        Route::delete('/rfid-blacklist/{id}', [
            'uses' => 'BlacklistController@delete'
        ]);

        // Untuk Users Token
        Route::get('/users-token', [
            'uses' => 'TokenCardController@view',
        ]);

        // get Dosen Inval
        Route::get('/users-token/dosen-inval/{id}', [
            'uses' => 'TokenCardController@getDosenInval'
        ]);

        Route::post('/users-token', [
            'uses' => 'TokenCardController@store'
        ]);

        Route::put('/users-token/{id}', [
            'uses' => 'TokenCardController@update'
        ]);

        Route::put('/users-token/use/{id}', [
            'uses' => 'TokenCardController@useToken'
        ]);

        Route::delete('/users-token/{id}', [
            'uses' => 'TokenCardController@delete'
        ]);

        Route::get('/users-token/aksi', [
            'uses' => 'TokenCardController@getAksiToken'
        ]);

        // Pencarian di Sistem Token
        // Cari dosen Pengganti
        Route::get('/dosen',[
            'uses' => 'TokenCardController@cariDosen'
        ]);

        // Cari dosen dan Mahasiswa
        Route::get('/cari', [
            'uses' => 'TokenCardController@cariNI'
        ]);
    });
});
