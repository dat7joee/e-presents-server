<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $table = 'data_blacklist';
    protected $primaryKey = 'id_blacklist';
    protected $fillable = ['rf_id'];
}
